The project is structured as follows:

    the data folder should contain the input data. Download the data from [here](https://drive.switch.ch/index.php/s/RrWjbj1UxhO5FKV)** and then unzip the data with `tar -zxvf data.tar.gz` into the folder. After unzipping the `data/` should contain multiple `.npz` files.

    the models folder contain models already trained with all their arguments in a yaml file, the best version of the model in a keras file, the history of the training in a csv table as well as in a plot in a png, and predictions for the test set from the best version of the model in a numpy file.

    the plots_data folder contains all the plots to visualize the data before and after preparation.

    the plots_models folder contains plots comparing all models and a table comparing the main features of the models.

    the plots_models_final folder contains all the plots to evaluate the final model.

    the src folder contains 3 files that are used in other files:
        data.py that load and prepare the data for training and evaluating the models.
        models.py which contains the 2 model classes.
        utils.py which contains several function used.

    Dampe_project_report.pdf presents the project and its results.

    data_visualization.py allows for making the plots of the plots_data folder.

    eval_final.py allows for making the plots and tables of the plots_models_final folder.

    eval.py allows for making the plots and tables of the plots_models folder.

    requirements.txt contains the needed libraries.

    train_cnn_energy.py allows for training models with the energy model class

    train_cnn.py allows for training models with the other model class

Models can be trained using train_cnn.py or train_cnn_energy.py with command line arguments that are presented in src/utils.py, such as the name of the model, the number of filters per layers, the number of mlp layers and others. 
Models can be compared using eval.py also with command line arguments that are presented in src/utils.py, such as the names of the models to compare, the arguments of the models to mention and others. 
More evaluating plots can be obtained for a model via eval_final.py using --models_names="model_name" for one model_name.