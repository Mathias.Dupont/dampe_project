"""Script to compare the models, inputs can be specified with command line arguments when
calling this file, see src/utils.py. A table with all the specified parameters 
of the models and some plots are made and stored in plots_models.
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml

from src.data import load_data
from src.utils import get_evaluation_args

# Gather the arguments
args = get_evaluation_args()

# Load the test set for evaluating
(data_test, energies_test, target_test, benchmark_test) = load_data()[2]

# For each model load the args used during training
model_args = []
for model_name in args.model_names:
    file_name = f"{args.model_dir}/{model_name}/args.yaml"
    with open(file_name) as f:
        model_args.append(yaml.safe_load(f))

# Pull out the important args
important_args = {
    a: [model_args[i][a] for i in range(len(args.model_names))] for a in args.important_args
}

# We dont actually need to reload the whole model, just its test outputs
losses = []
for model_name in args.model_names:
    out = np.load(f"{args.model_dir}/{model_name}/test_out.npy") 
    loss = np.mean(np.square(out - target_test))
    losses.append(loss)

# Create a table showing: model name, important args and test loss
df = pd.DataFrame()
df["Model"] = args.model_names
for k, v in important_args.items():
    df[k] = v
df["Test loss mse"] = losses
df.to_csv("plots_models/loss.csv", index=False)


# Plot the validation loss curves
fig, ax = plt.subplots()
for model_name in args.model_names:
    history = pd.read_csv(f"{args.model_dir}/{model_name}/history.csv")
    ax.plot(history["val_loss"], label=model_name)
ax.set_xlabel("Epoch")
ax.set_ylabel("Validation loss mse")
ax.legend()
fig.tight_layout()
fig.savefig("plots_models/val_loss.png")
plt.close()


# Plot the zoomed validation loss curves
for model_name in args.model_names:
    history = pd.read_csv(f"{args.model_dir}/{model_name}/history.csv")
    plt.plot(history["val_loss"], label=model_name)
plt.xlabel("Epoch")
plt.ylabel("Validation loss mse")
plt.legend()
plt.ylim([0,0.012])
plt.tight_layout()
plt.savefig("plots_models/val_loss_zoom.png")
plt.close()