"""Two model classes are present: CNN which is the model class without energy and 
CNN_energy which is the model class including the maximum and total energy. 
The models are used in train_cnn.py and train_cnn_energy respectively."""

import tensorflow as tf
import keras

class CNN(keras.Model):

    def __init__(   # Different parameters of the model
        self,
        output_shape: int,
        num_filters_per_layer: list,
        num_pooling_layers: int,
        num_mlp_layers: int,
        num_mlp_hidden: int,
        activation: str,
        dropout: float,
    )-> None:

            
        super().__init__()      # Convolutional layers with the number of filters in input
        self.conv_layers = []
        for filters in num_filters_per_layer:
            self.conv_layers.append(
                keras.layers.Conv2D(filters, kernel_size=(2,2), activation=activation)
            )
            for _ in range (num_pooling_layers):    # Adding pooling layers if specified
                self.conv_layers.append(keras.layers.AveragePooling2D(2,2))


        # We need to flatten the 3D output into a 1D tensor
        self.flatten = keras.layers.Flatten()

        # The mlp layers with the number of layers, the size of layers and the activation specified
        self.mlp_layers = []
        for _ in range(num_mlp_layers):
            self.mlp_layers.append(
                keras.layers.Dense(num_mlp_hidden, activation=activation)
            )
            self.mlp_layers.append(keras.layers.Dropout(dropout))

        # Last layer linear to have the final output
        self.output_layer = keras.layers.Dense(output_shape[-1], activation="linear")

    def call(self, w) -> tf.Tensor:
        """The forward pass of the model."""
        x=w[0]  # The x and y layers are separeted
        y=w[1]
        for layer in self.conv_layers: # The x and y layers are passed separatly
            x = layer(x)
            y = layer(y)
        x = self.flatten(x) # The results are flattened
        y = self.flatten(y) 
        z = keras.layers.concatenate((x, y), axis=1) # The two vectors are concatenated 
        for layer in self.mlp_layers: # The mlp part treats for all the targets
            z = layer(z)
        return self.output_layer(z)

class CNN_energy(keras.Model):

    def __init__(
        self,
        output_shape: int,
        num_filters_per_layer: list,
        num_pooling_layers: int,
        num_mlp_layers: int,
        num_mlp_hidden: int,
        activation: str,
        dropout: float,
    )-> None:

            
        super().__init__()
        self.conv_layers = []
        for filters in num_filters_per_layer:
            self.conv_layers.append(
                keras.layers.Conv2D(filters, kernel_size=(2,2), activation=activation)
            )
            for _ in range (num_pooling_layers):
                self.conv_layers.append(keras.layers.AveragePooling2D(2,2))

        self.flatten = keras.layers.Flatten()

        self.mlp_layers = []
        for _ in range(num_mlp_layers):
            self.mlp_layers.append(
                keras.layers.Dense(num_mlp_hidden, activation=activation)
            )
            self.mlp_layers.append(keras.layers.Dropout(dropout))

       
        self.output_layer = keras.layers.Dense(output_shape[-1], activation="linear")

    def call(self, w) -> tf.Tensor:
        images = w[0]   # Separation between the x and y images and the energies
        x = images[0]
        y = images[1]
        energy = w[1]
        for layer in self.conv_layers:
            x = layer(x)
            y = layer(y)
        x = self.flatten(x) 
        y = self.flatten(y) 
        z = keras.layers.concatenate((x, y, energy), axis=1) # The energies are concatened
        for layer in self.mlp_layers:                        # with the x and y outputs
            z = layer(z)
        return self.output_layer(z)
