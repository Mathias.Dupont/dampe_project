"""load_data: Load and prepare the data for the models training and evaluation
reverse_data: reverse the normalization of the targets
"""

from src.utils import get_input_data # Extract the data from the input files
import numpy as np

def load_data():
    calorimeter_images, calorimeter_data, data_target, benchmark_data = get_input_data()
    
    # Renormalization of the images
    calorimeter_images_norm = calorimeter_images / 255

    # Renormalization of the energy 
    energy = np.log(calorimeter_data)   # Log to reduce the tail of the distibutions
    energy_mean = np.mean(energy, axis = 0)
    energy_std = np.std(energy, axis = 0)
    energy_norm = (energy - energy_mean) / energy_std

    # Renormalization of the targets
    target_mean = np.mean(data_target, axis = 0)
    target_std = np.std(data_target, axis = 0)
    targets = (data_target - target_mean) / target_std
    benchmark_data = (benchmark_data - target_mean) / target_std

    # Separation between x and y images
    y_images = calorimeter_images_norm[:, ::2]
    x_images = calorimeter_images_norm[:, 1::2]

    # Separation between training (60%), validation (20%) and test data (20%)
    frac_val = 0.2
    n_val = int(len(x_images)*frac_val)

    x_validation = x_images[: n_val]
    y_validation = y_images[: n_val]
    energies_validation = energy_norm[: n_val]
    target_validation = targets[: n_val]
    data_validation = [x_validation, y_validation] # In a list to pass it to the model

    x_test = x_images[n_val : 2*n_val]
    y_test = y_images[n_val : 2*n_val]
    energies_test = energy_norm[n_val : 2*n_val]
    target_test = targets[n_val : 2*n_val]
    benchmark_test = benchmark_data[n_val : 2*n_val]
    data_test = [x_test, y_test]

    x_train = x_images[2*n_val :]
    y_train = y_images[2*n_val :]
    energies_train = energy_norm[2*n_val :]
    target_train = targets[2*n_val :]
    data_train = [x_train, y_train]

    return (data_train, energies_train, target_train), \
        (data_validation, energies_validation, target_validation), \
        (data_test, energies_test, target_test, benchmark_test), \
        (target_mean, target_std) 
# The mean and standard deviation are kept to allow for reversion


def reverse_data(data,mean,std):
    return (data * std) + mean

