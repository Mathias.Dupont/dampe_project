"""load_numpy_data_multifile: load the data from the input files
prepare_data: prepare the data in the good shape
get_input_data: uses the two previous functions to get the data and shuffle them

get_training_args: gather the inputs from the command line arguments to train the models
get_evaluation_args: gather the inputs from the command line arguments to evaluate the models
plot_history: plot the training history of the models"""

import numpy as np
import matplotlib.pyplot as plt
import glob
import argparse
from pathlib import Path
import keras

def load_numpy_data_multifile(filelist:list):
    allindata = []
    for fname in filelist:
        try:
            item = np.load(
                fname,
                allow_pickle=True,
                encoding="bytes"
            )['arr_0']  # all data
            if not item.shape[0]:
                print("[load_numpy_data_multifile] empy file, skipping: ", fname)
            else:
                allindata.append(item)
        except Exception as e:
            print("[load_numpy_data_multifile] failed to load file: ", fname)
            print("[load_numpy_data_multifile]  ... the error: ")
            print(e)
            print("[load_numpy_data_multifile]  ... skipping this file.")
    data = np.concatenate(allindata) if allindata else np.array(allindata)
    return data


def prepare_data(data):
    """

    The function prepares the data in the proper-shape numpy arrays

      data - normally, it is a numpy array of the data for training or prediction

    Data structure:
      [:,0] - calo images
      [:,1] - calo 2 variables  - bgoene (CALO total energy), maxbar (CALO energy of maximum bar)
      [:,2] - truth 4 variables - normally variables that are targeted at the regression optimisation,
      say x_bot, x_top, y_bot, y_top
      [:,3] - rec   4 variables - the same as above, but obtained from the standard BGO rec direction, 
      instead of the truth direction
    """

    caloimages = data[:, 0]
    calodata = data[:, 1]
    truthdata = data[:, 2]
    recdata = data[:, 3]

    # get tensor-like shape of the arrays
    caloimages = caloimages.tolist()
    calodata = calodata.tolist()
    truthdata = truthdata.tolist()
    recdata = recdata.tolist()

    # get a 3-dimensional array, 1st dimension - events, 2,3rd dimensions - image dimensions
    caloimages = np.array(caloimages,
                          dtype='float16') 
    calodata = np.array(calodata)  
    truthdata = np.array(truthdata)  
    recdata = np.array(recdata)  

    return {
        'caloimages': caloimages,
        'calodata': calodata,
        'truthdata': truthdata,
        'recdata': recdata,
    }

def get_input_data(data_path:str = 'data/tmp_*'):# -> tuple:
    # get all input data

    data_files = glob.glob(data_path)
    np.random.seed(1234)
    np.random.shuffle(data_files)
    data = load_numpy_data_multifile(data_files)
    # randomly shuffle the sample
    np.random.shuffle(data)

    # 'prepare' input data (in particular, normalize BGO image, etc.)
    data = prepare_data(data)

    return data['caloimages'], data['calodata'], data['truthdata'], data['recdata']

def get_training_args() -> argparse.Namespace:
    """Get the arguments needed for the training script."""

    # First we have to create a parser object
    parser = argparse.ArgumentParser()

    # Define the important paths for the project
    parser.add_argument(
        "--model_dir",  # How we access the argument when calling the file
        type=str,  # We must also define the type of argument, here it is a string
        default="models",  # The default value so you dont have to type it in every time
        help="Where to save trained models",  # A helpfull message
    )
    parser.add_argument(
        "--model_name",
        type=str,
        default="cnn",
        help="The name of the model",
    )


    # Arguments for the network
    parser.add_argument(
        "--num_filters_per_layer",
        type=str,
        default="8,16,32",
        help="A comma separated list of the number of filters per layer (for the convolutional part)",
    )
    parser.add_argument(
        "--num_pooling_layers",
        type=int,
        default=0,
        help="The number of pooling layers per convolutional layer"
    )
    parser.add_argument(
        "--num_mlp_layers",
        type=int,
        default=2,
        help="The number of MLP layers",
    )
    parser.add_argument(
        "--num_mlp_hidden",
        type=int,
        default=32,
        help="The size of the mlp layers",
    )
    parser.add_argument(
        "--activation",
        type=str,
        default="relu",
        help="The activation function",
    )
    parser.add_argument(
        "--dropout",
        type=float,
        default=0.0,
        help="The dropout rate",
    )
    parser.add_argument(
        "--norm_type",
        type=str,
        default="batch",
        help="The normalization type",
    )

  
    # Arguments for the model training
    parser.add_argument(
        "--loss_fn",
        type=str,
        default="mse",
        help="The loss function",
    )
    parser.add_argument(
        "--patience",
        type=int,
        default=5,
        help="The patience",
    )
    parser.add_argument(
        "--max_epochs",
        type=int,
        default=10,
        help="The maximum number of epochs",
    )
    parser.add_argument(
        "--batch_size",
        type=int,
        default=32,
        help="The batch size",
    )
    parser.add_argument(
        "--learning_rate",
        type=float,
        default=0.001,
        help="The learning rate",
    )
    parser.add_argument(
        "--weight_decay",
        type=float,
        default=0.0001,
        help="The weight decay",
    )
    parser.add_argument(
        "--optimizer",
        type=str,
        default="adamw",
        help="The optimizer",
    )

    # This now collects all arguments
    args = parser.parse_args()

    # We have to do some extra work to parse the list of integers
    args.num_filters_per_layer = [int(x) for x in args.num_filters_per_layer.split(",")]

    # Now we return the arguments
    return args

def get_evaluation_args() -> argparse.Namespace:
    """Load the arguments needed for the evaluation script."""
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--model_dir",
        type=str,
        default="models",
        help="Where the saved model are stored",
    )
    parser.add_argument(
        "--model_names",
        type=str,
        default="benchmark,cnn1,cnn2",
        help="A comma separated list of model names to load and compare",
    )
    parser.add_argument(
        "--important_args",
        type=str,
        default="num_filters_per_layer,num_mlp_layers,num_mlp_hidden,dropout",
        help="A comma separated list of args to include in the plots",
    )

    args = parser.parse_args()
    args.model_names = args.model_names.split(",")
    args.important_args = args.important_args.split(",")
    return args


# Plot of the training history
def plot_history(
    history: keras.callbacks.History,
    output_path: Path
) -> None:
    plt.plot(history.history["loss"], label="Train")
    plt.plot(history.history["val_loss"], label="Valid")
    plt.xlabel("Epoch")
    plt.ylabel("Loss")
    plt.legend()
    plt.savefig(output_path)
    plt.close()