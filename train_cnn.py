"""Train the cnn model class
Different inputs can be used with command line arguments when calling this file,
see src/utils.py for all the inputs"""
from pathlib import Path 

import numpy as np
import pandas as pd
import yaml 
import keras

from src.data import load_data
from src.models import CNN
from src.utils import get_training_args, plot_history

# Gather the arguments, look in the utils.py file to see how this works
args = get_training_args()

# 1) Load and prepare the data
# For neatness we offload all of this to a function in the src/data.py file

(data_train, energies_train, target_train), \
(data_validation, energies_validation, target_validation), \
(data_test, energies_test, target_test, benchmark_test),(__,__)  = load_data()




# 2) Define the model
# Here we create a cnn using all of the arguments we defined earlier
# To construct the cnn we use a class defined in the src/models.py file
model = CNN(
    output_shape=target_train[0].shape,
    num_filters_per_layer = args.num_filters_per_layer,
    num_pooling_layers = args.num_pooling_layers,
    num_mlp_layers = args.num_mlp_layers,
    num_mlp_hidden = args.num_mlp_hidden,
    activation = args.activation,
    dropout = args.dropout,
)

# To finalise building the model we must call it with some input
# Note that when doing the summary the output shapes are not defined
# This is because we are not using a fixed input shape
model(data_train)
model.summary()

# 3) Compile the model
# Compile the model using the arguments we defined earlier
model.compile(
    optimizer=keras.optimizers.get(
        {
            "class_name": args.optimizer,
            "learning_rate": args.learning_rate,
            "weight_decay": args.weight_decay,
        }
    ),
    loss=args.loss_fn,
)


# Each model will get its own directory/folder names after it
output_dir = Path(args.model_dir) / args.model_name
output_dir.mkdir(exist_ok=True, parents=True)  # Create the directory

# It is also incredibly useful to save all of the arguments that went into this
# script so we can reproduce the results later
# We use yaml to save (and later load) the dictionary of arguments
with open(output_dir / "args.yaml", "w") as f:
    yaml.dump(vars(args), f)

# 4) Train the model
history = model.fit(
    data_train,
    target_train,
    validation_data=(data_validation, target_validation),
    epochs=args.max_epochs,
    batch_size=args.batch_size,
    callbacks=[
        keras.callbacks.EarlyStopping(
            patience=args.patience,
            restore_best_weights=True,
        ),
        keras.callbacks.ModelCheckpoint(  # Note the callback here saves the model
            filepath=output_dir / "best.keras",
            save_best_only=True,
        ),
    ],
)

# Save the training history with a plot to go along with it
pd.DataFrame(history.history).to_csv(output_dir / "history.csv")
plot_history(history, output_dir / "history.png")

# 5) Predict on the test set
# Instead of evaluating the model on the test set here, we will do it in a separate
# script: `eval.py` This is because we might want to evaluate the model in different
# ways or we might want to evaluate multiple models at once.

model.load_weights(output_dir / "best.keras")  # Load the best version of the model
test_out = model.predict(data_test)
np.save(output_dir / "test_out.npy", test_out) # Save the test predictions
