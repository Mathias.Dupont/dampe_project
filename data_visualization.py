"""To plot the plots in the plots_data folder"""

from src.utils import get_input_data
import matplotlib.pyplot as plt
import numpy as np
from src.data import load_data

# Gather the data
calorimeter_images, calorimeter_data, data_target, benchmark_data = get_input_data() 

# Examples of data 
fig,axes = plt.subplots(nrows=1,ncols=3,figsize=(21,6))
for i,ax in enumerate(axes):
    ax.imshow(calorimeter_images[i])
    ax.set_title(f"Shower {i+1}")
    ax.set_ylabel("z layer")
    ax.set_xlabel("distance")
    ax.annotate(f"Total energy: {calorimeter_data[i][0]}",xy=(0,1),color='white')
    ax.annotate(f"Max energy: {calorimeter_data[i][1]}",xy=(0,2), color='white')
plt.savefig("plots_data/examples.png")
plt.close()

# Total data
total = np.zeros_like(calorimeter_images[0])
images = calorimeter_images/255
for i in range(calorimeter_images.shape[0]):
    total += images[i]
total = total/calorimeter_images.shape[0]
plt.imshow(total)
plt.ylabel("z layer")
plt.xlabel("distance")
plt.title("All data")
plt.savefig("plots_data/all_data.png")
plt.close()



# Plot for the total energy distribution
total_energy = calorimeter_data[:,0]
plt.hist(total_energy, bins=100, label="Total energy") 
plt.legend()
plt.xlabel("Total energy [eV]")
plt.ylabel("Number of images")
plt.savefig("plots_data/Total_energy.png")
plt.close()

# Plot for the maximum energy distribution
max_energy = calorimeter_data[:,1]
plt.hist(max_energy, bins=100, label="Maximum energy") 
plt.legend()
plt.xlabel("Maximum energy [eV]")
plt.ylabel("Number of images")
plt.savefig("plots_data/Maximum_energy.png")
plt.close()


# Plots of the targets
plt.hist(data_target[:,0], bins=100, label='x bottom')
plt.xlabel('x distance')
plt.ylabel('Number of images')
plt.legend()
plt.savefig('plots_data/x_bottom.png')
plt.close()

plt.hist(data_target[:,1], bins=100, label='x top')
plt.xlabel('x distance')
plt.ylabel('Number of images')
plt.legend()
plt.savefig('plots_data/x_top.png')
plt.close()

plt.hist(data_target[:,2], bins=100, label='y bottom')
plt.xlabel('y distance')
plt.ylabel('Number of images')
plt.legend()
plt.savefig('plots_data/y_bottom.png')
plt.close()

plt.hist(data_target[:,3], bins=100, label='y top')
plt.xlabel('y distance')
plt.ylabel('Number of images')
plt.legend()
plt.savefig('plots_data/y_top.png')
plt.close()



# Plots for examples of separated x and y data
(data_train, energies_train, target_train), \
(data_validation, energies_validation, target_validation), \
(data_test, energies_test, target_test, benchmark_test), (__,__)  = load_data()

x_images = data_validation[0]
plt.imshow(x_images[0], extent=[-400, 400, 6, 0], aspect="auto")
plt.plot(data_target[0][0:2], [0, 6], label="target")
plt.legend()
plt.ylabel("z layer")
plt.xlabel("x distance")
plt.savefig('plots_data/example_x.png')
plt.close()

y_images = data_validation[1]
plt.imshow(y_images[0], extent=[-400, 400, 6, 0], aspect="auto")
plt.plot(data_target[0][2:4], [0, 6], label="target")
plt.legend()
plt.ylabel("z layer")
plt.xlabel("y distance")
plt.savefig('plots_data/example_y.png')
plt.close()

# Plot of the processed energy
energy = np.concatenate((energies_test, energies_train, energies_validation))
total_energy = energy[:,0]
plt.hist(total_energy, bins=100, label="Total energy normalized") 
plt.legend()
plt.xlabel("Total energy")
plt.ylabel("Number of images")
plt.savefig("plots_data/Total_energy_processed.png")
plt.close()


max_energy = energy[:,1]
plt.hist(max_energy, bins=100, label="Maximum energy normalized") 
plt.legend()
plt.xlabel("Maximum energy")
plt.ylabel("Number of images")
plt.savefig("plots_data/Maximum_energy_processed.png")
plt.close()