"""Script to evaluate the final model, the model name should be specified with --model_name="model_name",
other inputs can be specified with command line arguments when calling this file, see src/utils.py. 
Scatter plots, relative error plots and binned error plots for all the targets are made 
and stored in plots_models_final. A table that summarize the final model with the test loss and 
a plot of the validation loss is also present.

"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml
from scipy.stats import binned_statistic

from src.data import load_data, reverse_data
from src.utils import get_evaluation_args

# Gather the arguments
args = get_evaluation_args()

# Load the data for evaluating
(__,__,__),(__,__,__),(data_test, energies_test, target_test, benchmark_test)\
    ,(mean,std) = load_data()


# For the model load the args used during training
model_args = []
for model_name in args.model_names:
    file_name = f"{args.model_dir}/{model_name}/args.yaml"
    with open(file_name) as f:
        model_args.append(yaml.safe_load(f))

# Pull out the important args
important_args = {
    a: [model_args[i][a] for i in range(len(args.model_names))] for a in args.important_args
}

# We dont actually need to reload the whole model, just its test outputs
losses = []
for model_name in args.model_names:
    out = np.load(f"{args.model_dir}/{model_name}/test_out.npy") 
    loss = np.mean(np.square(out - target_test))
    losses.append(loss)

# Create a table showing: model name, important args, accuracy and loss
df = pd.DataFrame()
df["Model"] = args.model_names
for k, v in important_args.items():
    df[k] = v
df["Test loss mse"] = losses
df.to_csv("plots_models_final/loss_final.csv", index=False)


# Plot the loss curve
fig, ax = plt.subplots()
for model_name in args.model_names:
    history = pd.read_csv(f"{args.model_dir}/{model_name}/history.csv")
    ax.plot(history["val_loss"], label=model_name)
ax.set_xlabel("Epoch")
ax.set_ylabel("Validation loss mse")
ax.legend()
fig.tight_layout()
fig.savefig("plots_models_final/val_loss_final.png")
plt.close()


# Reverse the targets
target = reverse_data(target_test,mean,std)
prediction = reverse_data(out,mean,std)



# Plot of the relative error
relative_err_x_bot = (target[:,0]-prediction[:,0])/target[:,0]
relative_err_x_bot=np.clip(a_min=-1,a_max=1,a=relative_err_x_bot) # Clip to see better around 0
plt.hist(relative_err_x_bot, bins=300, label="x bottom")
plt.xlabel("Relative error")
plt.ylabel("Number of predictions")
plt.legend()
plt.vlines(0,0,3000,'r',linestyles='dotted')
plt.savefig("plots_models_final/relative_err_x_bot.png")
plt.close()

relative_err_x_top = (target[:,1]-prediction[:,1])/target[:,1]
relative_err_x_top=np.clip(a_min=-1,a_max=1,a=relative_err_x_top)
plt.hist(relative_err_x_top, bins=300, label="x top")
plt.xlabel("Relative error")
plt.ylabel("Number of predictions")
plt.legend()
plt.vlines(0,0,3000,'r',linestyles='dotted')
plt.savefig("plots_models_final/relative_err_x_top.png")
plt.close()

relative_err_y_bot = (target[:,2]-prediction[:,2])/target[:,2]
relative_err_y_bot=np.clip(a_min=-1,a_max=1,a=relative_err_y_bot)
plt.hist(relative_err_y_bot, bins=300, label="y bottom")
plt.xlabel("Relative error")
plt.ylabel("Number of predictions")
plt.legend()
plt.vlines(0,0,3000,'r',linestyles='dotted')
plt.savefig("plots_models_final/relative_err_y_bot.png")
plt.close()

relative_err_y_top = (target[:,3]-prediction[:,3])/target[:,3]
relative_err_y_top=np.clip(a_min=-1,a_max=1,a=relative_err_y_top)
plt.hist(relative_err_y_top, bins=300, label="y top")
plt.xlabel("Relative error")
plt.ylabel("Number of predictions")
plt.legend()
plt.vlines(0,0,3000,'r',linestyles='dotted')
plt.savefig("plots_models_final/relative_err_y_top.png")
plt.close()

# Scatter plots
plt.plot(target[:,0],prediction[:,0],'.', label="x bottom")
plt.plot(target[:,0],target[:,0],'r', label="Identity")
plt.xlabel("True target")
plt.ylabel("Predicted target")
plt.legend()
plt.savefig("plots_models_final/scatter_x_bot.png")
plt.close()

plt.plot(target[:,1],prediction[:,1],'.', label="x top")
plt.plot(target[:,1],target[:,1],'r', label="Identity")
plt.xlabel("True target")
plt.ylabel("Predicted target")
plt.legend()
plt.savefig("plots_models_final/scatter_x_top.png")
plt.close()

plt.plot(target[:,2],prediction[:,2],'.', label="y bottom")
plt.plot(target[:,2],target[:,2],'r', label="Identity")
plt.xlabel("True target")
plt.ylabel("Predicted target")
plt.legend()
plt.savefig("plots_models_final/scatter_y_bot.png")
plt.close()

plt.plot(target[:,3],prediction[:,3],'.', label="y top")
plt.plot(target[:,3],target[:,3],'r', label="Identity")
plt.xlabel("True target")
plt.ylabel("Predicted target")
plt.legend()
plt.savefig("plots_models_final/scatter_y_top.png")
plt.close()

# Plot of the loss binned in the true offset
prediction_benchmark = reverse_data(benchmark_test,mean,std)

bins = np.linspace(-400, 400, 30)
bins_top = np.linspace(np.min(target[:,1]),np.max(target[:,1]), 30) # The top coordinates have different limits

# Compute the squared error
mse_model_x_bot = np.square(prediction[:,0] - target[:,0])
mse_benchmark_x_bot = np.square(prediction_benchmark[:,0] - target[:,0])

mse_model_x_top = np.square(prediction[:,1] - target[:,1])
mse_benchmark_x_top = np.square(prediction_benchmark[:,1] - target[:,1])

mse_model_y_bot = np.square(prediction[:,2] - target[:,2])
mse_benchmark_y_bot = np.square(prediction_benchmark[:,2] - target[:,2])

mse_model_y_top = np.square(prediction[:,3] - target[:,3])
mse_benchmark_y_top = np.square(prediction_benchmark[:,3] - target[:,3])

# Calculate the mean and standard deviation of the prediction error in each bin
mean_m_x_bot, _, _ = binned_statistic(target[:, 0], mse_model_x_bot, statistic="mean", bins=bins)
std_m_x_bot, _, _ = binned_statistic(target[:, 0], mse_model_x_bot, statistic="std", bins=bins)
mean_b_x_bot, _, _ = binned_statistic(target[:, 0], mse_benchmark_x_bot, statistic="mean", bins=bins)
std_b_x_bot, _, _ = binned_statistic(target[:, 0], mse_benchmark_x_bot, statistic="std", bins=bins)

mean_m_x_top, _, _ = binned_statistic(target[:, 1], mse_model_x_top, statistic="mean", bins=bins_top)
std_m_x_top, _, _ = binned_statistic(target[:, 1], mse_model_x_top, statistic="std", bins=bins_top)
mean_b_x_top, _, _ = binned_statistic(target[:, 1], mse_benchmark_x_top, statistic="mean", bins=bins_top)
std_b_x_top, _, _ = binned_statistic(target[:, 1], mse_benchmark_x_top, statistic="std", bins=bins_top)

mean_m_y_bot, _, _ = binned_statistic(target[:, 2], mse_model_y_bot, statistic="mean", bins=bins)
std_m_y_bot, _, _ = binned_statistic(target[:, 2], mse_model_y_bot, statistic="std", bins=bins)
mean_b_y_bot, _, _ = binned_statistic(target[:, 2], mse_benchmark_y_bot, statistic="mean", bins=bins)
std_b_y_bot, _, _ = binned_statistic(target[:, 2], mse_benchmark_y_bot, statistic="std", bins=bins)

mean_m_y_top, _, _ = binned_statistic(target[:, 3], mse_model_y_top, statistic="mean", bins=bins_top)
std_m_y_top, _, _ = binned_statistic(target[:, 3], mse_model_y_top, statistic="std", bins=bins_top)
mean_b_y_top, _, _ = binned_statistic(target[:, 3], mse_benchmark_y_top, statistic="mean", bins=bins_top)
std_b_y_top, _, _ = binned_statistic(target[:, 3], mse_benchmark_y_top, statistic="std", bins=bins_top)

# Calculate the bin centers
bin_centers = 0.5 * (bins[1:] + bins[:-1])
bin_centers_top = 0.5 * (bins_top[1:] + bins_top[:-1])

# Plot the results
plt.errorbar(bin_centers, mean_b_x_bot, yerr=std_b_x_bot, fmt="o", label="Benchmark x bottom")
plt.errorbar(bin_centers, mean_m_x_bot, yerr=std_m_x_bot, fmt="o", label="Model x bottom")
plt.xlabel("Truth x coordinate")
plt.ylabel("Prediction Error")
plt.legend()
plt.savefig("plots_models_final/error_x_bot.png")
plt.close()

plt.errorbar(bin_centers_top, mean_b_x_top, yerr=std_b_x_top, fmt="o", label="Benchmark x top")
plt.errorbar(bin_centers_top, mean_m_x_top, yerr=std_m_x_top, fmt="o", label="Model x top")
plt.xlabel("Truth x coordinate")
plt.ylabel("Prediction Error")
plt.legend()
plt.savefig("plots_models_final/error_x_top.png")
plt.close()

plt.errorbar(bin_centers, mean_b_y_bot, yerr=std_b_y_bot, fmt="o", label="Benchmark y bottom")
plt.errorbar(bin_centers, mean_m_y_bot, yerr=std_m_y_bot, fmt="o", label="Model y bottom")
plt.xlabel("Truth y coordinate")
plt.ylabel("Prediction Error")
plt.legend()
plt.savefig("plots_models_final/error_y_bot.png")
plt.close()

plt.errorbar(bin_centers_top, mean_b_y_top, yerr=std_b_y_top, fmt="o", label="Benchmark y top")
plt.errorbar(bin_centers_top, mean_m_y_top, yerr=std_m_y_top, fmt="o", label="Model y top")
plt.xlabel("Truth y coordinate")
plt.ylabel("Prediction Error")
plt.legend()
plt.savefig("plots_models_final/error_y_top.png")
plt.close()
